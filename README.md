Magento 2 Manda� Shipping Method

**Manual**

Caso n�o exista, crie uma pasta "app/code/"<br />
Coloca o m�dulo dentro dessa pasta

Pela linha de comando digite<br />
`php bin/magento setup:upgrade`

**Composer**

- `composer config repositories.mandae-magento2 git https://bitbucket.org:mandae/magento-calculo-frete-2.0.git`<br />
- `composer require mandae/magento-calculo-frete-2.0:dev-master`<br />
- `php bin/magento setup:upgrade`