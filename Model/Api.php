<?php
/**
 * Mandaê
 *
 * @category   Mandae
 * @package    Mandae_Shipping
 * @author     Thiago Contardi
 * @copyright  Copyright (c) 2018 Bizcommerce
 */

namespace Mandae\Shipping\Model;

use Magento\Framework\DataObject;
use Zend\Http\Headers;
use Zend\Http\Request;
use Zend\Http\Client;

class Api
{
    const URL_API = 'https://api.mandae.com.br/v2/';
    const SANDBOX_URL_API = 'https://sandbox.api.mandae.com.br/v2/';

    protected $_helper;

    protected $_scopeConfig;

    public function __construct(
        \Mandae\Shipping\Helper\Data $helper,
        \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig
    )
    {
        $this->_scopeConfig = $scopeConfig;
        $this->_helper = $helper;
    }

    /**
     * @param $data
     * @param \Mandae\Shipping\Model\Carrier $carrier
     * @return \Zend\Http\Response
     */
    public function shippingRate(\Mandae\Shipping\Model\Carrier $carrier, $data)
    {
        $postcode = $data['postcode'];
        $url = $this->getUrl() . 'postalcodes/' . $postcode . '/rates';

        $declaredValue = $carrier->getConfigData('use_declared_value') ? $data['subtotal'] : '0.00';

        // e.g. { "declaredValue": 20.00, "weight" : 2, "height" : 100, "width" : 10,"length": 10 }
        $args = array(
            'declaredValue' => $declaredValue,
            'weight' => $data['weight'],
            'height' => $data['height'],
            'width' => $data['width'],
            'length' => $data['length'],
        );

        $data = new DataObject();
        $data->setData($args);

        $this->getHelper()->log('E: ' . $url . '. DATA:' . json_encode($args));
        $response = $this->request($carrier, $url, 'POST', $data->getData());
        $this->getHelper()->log('R HTTP ' . $response->getStatusCode());
        $this->getHelper()->log('R:' . $response->getBody());
        $content = $response->getBody();

        if ($content) {
            return json_decode($content);
        } else {
            return null;
        }
    }


    /**
     *
     * @param \Mandae\Shipping\Model\Carrier $carrier
     * @return mixed
     */
    public function tracking(\Mandae\Shipping\Model\Carrier $carrier, $trackingCode)
    {
        $result = null;

        $url = $this->getUrl() . 'trackings/' . $trackingCode;

        $this->getHelper()->log('TRACKING E: ' . $url);
        $tracking = $this->request($carrier, $url);
        $this->getHelper()->log('TRACKING R:' . json_encode($tracking));

        $content = $tracking->getContent();
        if ($content) {
            $result = json_decode($content);
        }

        return $result;

    }

    /**
     * @param \Mandae\Shipping\Model\Carrier $carrier
     * @param $url
     * @param string $method
     * @param null $data
     *
     * @return \Zend\Http\Response
     */
    protected function request(\Mandae\Shipping\Model\Carrier $carrier, $url, $method = 'GET', $data = null)
    {
        $request = new Request();
        $request->setUri($url);
        $request->setMethod($method);

        $headers = new Headers();
        $headers->addHeaders([
            'Content-Type' => 'application/json',
            'Authorization' => $carrier->getConfigData('token')
        ]);

        $request->setHeaders($headers);

        if ($data) {
            $request->setContent(json_encode($data));
        }

        $client = new Client();
        $response = $client->send($request);

        // initialize server and set URI
        return $response;
    }

    /**
     * @return \Mandae\Shipping\Helper\Data
     */
    protected function getHelper()
    {
        return $this->_helper;
    }

    protected function getUrl()
    {
        return ($this->getHelper()->isSandbox()) ? self::SANDBOX_URL_API : self::URL_API;
    }
}