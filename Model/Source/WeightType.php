<?php
/**
 * @package     Mandae_Shipping
 * @author      Mandaê
 * @copyright   Mandaê - https://www.mandae.com.br
 * @license     https://opensource.org/licenses/AFL-3.0  Academic Free License 3.0 | Open Source Initiative
 */

namespace Mandae\Shipping\Model\Source;

class WeightType implements \Magento\Framework\Option\ArrayInterface
{
    /**
     * Options getter
     *
     * @return array
     */
    public function toOptionArray()
    {
        return [
            ['value' => 'g', 'label' => __('Gram')],
            ['value' => 'kg', 'label' => __('Kilogram')]
        ];
    }

    /**
     * Get options in "key-value" format
     *
     * @return array
     */
    public function toArray()
    {
        return ['g' => __('Gram'), 'kg' => __('Kilogram')];
    }
}
