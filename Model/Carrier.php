<?php
/**
 * @package     Mandae_Shipping
 * @author      Mandaê
 * @copyright   Mandaê - https://www.mandae.com.br
 * @license     https://opensource.org/licenses/AFL-3.0  Academic Free License 3.0 | Open Source Initiative
 */

namespace Mandae\Shipping\Model;

use Magento\Framework\Xml\Security;
use Magento\Quote\Model\Quote\Address\RateRequest;
use Magento\Shipping\Model\Carrier\AbstractCarrierOnline;
use Magento\Shipping\Model\Rate\Result;
use Magento\Shipping\Model\Tracking\Result\ErrorFactory;
use Mandae\Shipping\Model\Source\Methods;

class Carrier extends AbstractCarrierOnline implements \Magento\Shipping\Model\Carrier\CarrierInterface
{
    /**
     * Code of the carrier
     *
     * @var string
     */
    const CODE = 'mandae';
    const WEIGHT_ROUND = 2;

    /**
     * Code of the carrier
     *
     * @var string
     */
    protected $_code = self::CODE;

    /**
     * @var \Magento\Catalog\Api\ProductRepositoryInterface
     */
    protected $productRepository;

    /**
     * @var \Magento\Shipping\Model\Rate\ResultFactory
     */
    protected $_rateResultFactory;

    /**
     * Rate result data
     *
     * @var Result|null
     */
    protected $_result = null;

    /**
     * @var \Mandae\Shipping\Model\Api
     */
    protected $api;

    /**
     * @var \Mandae\Shipping\Helper\Data
     */
    protected $helper;

    /**
     * @param \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig
     * @param \Magento\Quote\Model\Quote\Address\RateResult\ErrorFactory $rateErrorFactory
     * @param \Psr\Log\LoggerInterface $logger
     * @param Security $xmlSecurity
     * @param \Magento\Shipping\Model\Simplexml\ElementFactory $xmlElFactory
     * @param \Magento\Shipping\Model\Rate\ResultFactory $rateFactory
     * @param \Magento\Quote\Model\Quote\Address\RateResult\MethodFactory $rateMethodFactory
     * @param \Magento\Shipping\Model\Tracking\ResultFactory $trackFactory
     * @param \Magento\Shipping\Model\Tracking\Result\ErrorFactory $trackErrorFactory
     * @param \Magento\Shipping\Model\Tracking\Result\StatusFactory $trackStatusFactory
     * @param \Magento\Directory\Model\RegionFactory $regionFactory
     * @param \Magento\Directory\Model\CountryFactory $countryFactory
     * @param \Magento\Directory\Model\CurrencyFactory $currencyFactory
     * @param \Magento\Directory\Helper\Data $directoryData
     * @param \Magento\CatalogInventory\Api\StockRegistryInterface $stockRegistry
     * @param array $data
     *
     * @param \Mandae\Shipping\Helper\Data $helper
     * @param \Magento\Catalog\Model\ProductFactory $product
     *
     * @SuppressWarnings(PHPMD.ExcessiveParameterList)
     */
    public function __construct(
        \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig,
        \Magento\Quote\Model\Quote\Address\RateResult\ErrorFactory $rateErrorFactory,
        \Psr\Log\LoggerInterface $logger,
        Security $xmlSecurity,
        \Magento\Shipping\Model\Simplexml\ElementFactory $xmlElFactory,
        \Magento\Shipping\Model\Rate\ResultFactory $rateFactory,
        \Magento\Quote\Model\Quote\Address\RateResult\MethodFactory $rateMethodFactory,
        \Magento\Shipping\Model\Tracking\ResultFactory $trackFactory,
        \Magento\Shipping\Model\Tracking\Result\ErrorFactory $trackErrorFactory,
        \Magento\Shipping\Model\Tracking\Result\StatusFactory $trackStatusFactory,
        \Magento\Directory\Model\RegionFactory $regionFactory,
        \Magento\Directory\Model\CountryFactory $countryFactory,
        \Magento\Directory\Model\CurrencyFactory $currencyFactory,
        \Magento\Directory\Helper\Data $directoryData,
        \Magento\CatalogInventory\Api\StockRegistryInterface $stockRegistry,
        \Mandae\Shipping\Helper\Data $helper,
        \Mandae\Shipping\Model\Api $api,
        \Magento\Catalog\Api\ProductRepositoryInterface $productRepository,
        array $data = []
    )
    {
        parent::__construct(
            $scopeConfig,
            $rateErrorFactory,
            $logger,
            $xmlSecurity,
            $xmlElFactory,
            $rateFactory,
            $rateMethodFactory,
            $trackFactory,
            $trackErrorFactory,
            $trackStatusFactory,
            $regionFactory,
            $countryFactory,
            $currencyFactory,
            $directoryData,
            $stockRegistry,
            $data
        );

        $this->_rateFactory = $rateFactory;
        $this->productRepository = $productRepository;
        $this->helper = $helper;
        $this->api = $api;
    }


    /**
     * Coleta os valores e exibe
     *
     * @param \Magento\Quote\Model\Quote\Address\RateRequest $request
     * @return \Magento\Shipping\Model\Rate\Result | boolean
     */
    public function collectRates(RateRequest $request)
    {
        if (!$this->canCollectRates()) {
            return $this->getErrorMessage();
        }

        $this->appendMethod($request);

        return $this->_result;
    }

    /**
     * @param \Magento\Quote\Model\Quote\Address\RateRequest $request
     * @return false|Result
     */
    protected function appendMethod(RateRequest $request)
    {
        /** @var \Magento\Shipping\Model\Rate\Result _result */
        $this->_result = $this->_rateFactory->create();

        try {
            $data = array();
            $allowFreeShipping = $this->getConfigData('allow_free_shipping');
            $freeShippingService = explode(',', $this->getConfigData('free_shipping_service') ?? '');
            $allowedMethods = explode(',', $this->getConfigData('allowed_methods') ?? '');

            // Clean Zip Code
            $destPostcode = preg_replace('/[^0-9]/', '', $request->getDestPostcode());

            // Weight
            $weight = (float)$request->getPackageWeight();
            if ($this->getConfigData('weight_type') == 'g') {
                $weight = $weight / 1000;
            }
            $weight = number_format($weight, self::WEIGHT_ROUND, '.', '');

            $subtotal = 0;
            $items = $this->getAllItems($request);

            /* @var \Magento\Quote\Model\Quote\Item $item */
            foreach ($items as $item) {
                //Sum quote price, without virtual and downloadable products
                $subtotal += $item->getBaseRowTotal();
            }
            $totalValue = number_format($subtotal, 2, '.', '');

            $data['postcode'] = $destPostcode;
            $dimensions = $this->getDimensions($items);

            $data['weight'] = $weight;
            $data['width'] = $dimensions['width'];
            $data['height'] = $dimensions['height'];
            $data['length'] = $dimensions['length'];
            $data['subtotal'] = $totalValue;

            /** @var \Mandae\Shipping\Model\Api $api */
            $api = $this->getApi();
            $response = $api->shippingRate($this, $data);

            if (!$response) {
                throw new \Exception(__('Erro no retorno da consulta'));
            }

            if (property_exists($response, 'error')) {

                //Log the error code and error message
                $this->getHelper()->log('Error (' . $response->error->code . '): ' . $response->error->message);
                $error = $this->getErrorMessage();
                $this->_result->append($error);

            } else {
                if (!property_exists($response, 'shippingServices')) {
                    throw new \Exception(__('Erro no retorno da consulta do Web Service'));
                }

                $shippingServices = $response->shippingServices;

                foreach ($shippingServices as $shiping) {
                    //Message to title
                    $methodTitle = $this->getConfigData('message_deadline');

                    $deliveryTime = $shiping->days;
                    $shippingPrice = $shiping->price;
                    $shippingName = $shiping->name;
                    $shipingCode = isset($shiping->id) ? $shiping->id : $this->getHelper()->slugify($shippingName);

                    if (!in_array($shipingCode, $allowedMethods))
                        continue;

                    $addDeadline = 0;
                    $deadline = 0;
                    $avaiabilityAttribute = $this->getConfigData('attribute_add_deadline');

                    if ($avaiabilityAttribute) {
                        /* @var $item \Magento\Quote\Model\Quote\Item */
                        foreach ($items as $item) {
                            $productId = $item->getProduct()->getId();

                            //If different of bundle because bundle have a lot of products
                            if ($item->getProductType() != 'bundle') {
                                $childProduct = null;
                                if ($item->getProductType() == 'configurable') {
                                    /** @var \Magento\Catalog\Model\Product $childProduct */
                                    $childProduct = $this->productRepository->get($item->getSku());
                                }

                                /** @var \Magento\Catalog\Model\Product $product */
                                $product = $this->productRepository->getById($productId);
                                $deadline = $this->getAddDeliveryDays($product, $item, $deadline);

                                //Se houver produto filho e o produto tiver prazo, adicionará esse prazo
                                if ($childProduct && $childProduct->getId()) {
                                    $deadline = $this->getAddDeliveryDays($product, $item, $deadline);
                                }

                            } else {
                                $productOptions = $item->getQtyOptions();
                                if ($productOptions) {
                                    //If product is bundle, need to verify if all products are in stock
                                    if ($item->getProductType() == 'bundle') {
                                        foreach ($productOptions as $productId => $product) {
                                            /** @var \Magento\Catalog\Model\Product $product */
                                            $product = $this->productRepository->getById($productId);
                                            $deadline = $this->getAddDeliveryDays($product, $item, $deadline);
                                        }
                                    }
                                }
                            }
                        }

                        $addDeadline += $deadline;

                    }

                    $deliveryTime = ($deliveryTime + $addDeadline);
                    $methodTitle = sprintf($methodTitle, $shippingName, $deliveryTime);

                    if ($this->getConfigData('handling_fee')) {
                        $shippingPrice = $this->getFinalPriceWithHandlingFee($shippingPrice);
                    }

                    $methodTitle = $methodTitle ?: $this->getConfigData('title');

                    /** @var \Magento\Quote\Model\Quote\Address\RateResult\Method $method */
                    $method = $this->_rateMethodFactory->create();
                    $method->setCarrier($this->getCarrierCode());
                    $method->setCarrierTitle($this->getConfigData('title'));
                    $method->setMethod($shipingCode);
                    $method->setMethodTitle($methodTitle);
                    $method->setCost($shippingPrice);
                    $method->setPrice($shippingPrice);

                    if (
                        $allowFreeShipping
                        && $request->getFreeShipping() === true
                        && in_array($shipingCode, $freeShippingService)
                    ) {
                        $method->setPrice(0);
                        $method->setCost(0);
                    }
                    
                    $this->_result->append($method);
                }

            }

        } catch (\Exception $e) {
            $this->getHelper()->log($e->getMessage());
        }
        return $this->_result;
    }

    /**
     * Processing additional validation to check if carrier applicable.
     *
     * @param \Magento\Framework\DataObject $request
     * @return $this|bool|\Magento\Framework\DataObject
     * @SuppressWarnings(PHPMD.CyclomaticComplexity)
     * @SuppressWarnings(PHPMD.NPathComplexity)
     */
    public function processAdditionalValidation(\Magento\Framework\DataObject $request)
    {
        //Skip by item validation if there is no items in request
        if (!count($this->getAllItems($request))) {
            return $this;
        }

        $showMethod = $this->getConfigData('showmethod');

        if (!preg_match('/^([0-9]{5}\-?[0-9]{3})$/', $request->getDestPostcode() ?? '')) {

            if ($showMethod) {
                $errorMsg = __('This shipping method is not available. Please specify the zip code.');

                $error = $this->_rateErrorFactory->create();
                $error->setCarrier($this->_code);
                $error->setCarrierTitle($this->getConfigData('title'));
                $error->setErrorMessage($errorMsg);

                return $error;
            }

            return false;
        }

        return $this;
    }

    /**
     * Processing additional validation to check is carrier applicable.
     *
     * @param \Magento\Framework\DataObject $request
     * @return $this|bool|\Magento\Framework\DataObject
     * @deprecated 100.2.6
     * @SuppressWarnings(PHPMD.UnusedFormalParameter)
     */
    public function proccessAdditionalValidation(\Magento\Framework\DataObject $request)
    {
        return $this->processAdditionalValidation($request);
    }

    /**
     * Get tracking
     *
     * @param string|string[] $trackings
     * @return Result|null
     */
    public function getTracking($trackings)
    {
        $carrierTitle = $this->getConfigData('title');
        $counter = 0;

        /** @var \Mandae\Shipping\Model\Api $api */
        $api = $this->getApi();

        foreach ((array)$trackings as $trackingCode) {
            $progress = array();

            try {

                $response = $api->tracking($this, $trackingCode);

                if (!$response) {
                    throw new \Exception(__('Erro no retorno do Web Service'));
                }

                if (
                property_exists($response, 'error')
                ) {
                    throw new \Exception(
                        __('Erro no retorno do Web Service') . '. ' .
                        'Error (' . $response->error->code . '): ' . $response->error->message
                    );
                }

                $events = $response->events;
                $status = property_exists($response, 'carrierName') ? $response->carrierName : null;

                if (!empty($events)) {

                    $i = 0;
                    foreach ($events as $item) {
                        $date = null;
                        $time = null;

                        $dateTime = trim($item->date);
                        $name = trim($item->name);
                        $description = trim($item->description);

                        if ($i == 0) {
                            $status = $name;
                        }

                        if (strpos($dateTime, ' ') !== false) {
                            list($date, $time) = explode(' ', $dateTime);
                        }

                        $progressItem = array(
                            'activity' => $name,
                            'deliverydate' => $date,
                            'deliverytime' => $time,
                            'deliverylocation' => $description
                        );

                        array_push($progress, $progressItem);
                        $i++;
                    }

                }

                $track = array(
                    'status' => $status,
                    'progressdetail' => $this->processTrackingDetails($progress)
                );

                $tracking = $this->_trackStatusFactory->create();
                $tracking->setCarrier($this->getCarrierCode());
                $tracking->setCarrierTitle($carrierTitle);
                $tracking->setTracking($trackingCode);
                $tracking->addData($track);
                $this->getResult()->append($tracking);
                $counter++;

            } catch (\Exception $e) {
                $this->getHelper()->log($e->getMessage());
                $this->appendTrackingError($trackingCode, $e->getMessage());
            }

        }

        return $this->getResult();
    }

    /**
     * Append error message to rate result instance
     * @param string $trackingValue
     * @param string $errorMessage
     */
    protected function appendTrackingError($trackingValue, $errorMessage)
    {
        /** @var \Magento\Shipping\Model\Tracking\Result\Error $error */
        $error = $this->_trackErrorFactory->create();
        $error->setCarrier($this->getCarrierCode());
        $error->setCarrierTitle($this->getConfigData('title'));
        $error->setTracking($trackingValue);
        $error->setErrorMessage($errorMessage);
        $result = $this->getResult();
        $result->append($error);
    }

    /**
     * @param array $progressDetail
     * @return array
     */
    protected function processTrackingDetails($progressDetail)
    {
        if (empty($progressDetail)) {
            $progressDetail = array(
                array(
                    'deliverydate' => date('Y-m-d'),
                    'deliverytime' => date('00:00:00'),
                    'deliverylocation' => __('Nenhum item encontrado com esse código')
                )
            );
        }

        return $progressDetail;
    }

    /**
     * @return \Mandae\Shipping\Helper\Data
     */
    public function getHelper()
    {
        return $this->helper;
    }

    /**
     * @return \Mandae\Shipping\Model\Api
     */
    public function getApi()
    {
        return $this->api;
    }

    /**
     * Get allowed shipping methods
     *
     * @return array
     */
    public function getAllowedMethods()
    {
        $allowed = explode(',', $this->getConfigData('allowed_methods') ?? '');
        $methods = new Methods();
        $mandaeMethods = $methods->toArray();
        $arr = [];
        foreach ($allowed as $k) {
            $arr[$k] = $mandaeMethods[$k];
        }

        return $arr;
    }

    /**
     * Do shipment request to carrier web service, obtain Print Shipping Labels and process errors in response
     *
     * @param \Magento\Framework\DataObject $request
     * @return \Magento\Framework\DataObject
     */
    protected function _doShipmentRequest(\Magento\Framework\DataObject $request)
    {
        $this->_prepareShipmentRequest($request);
        $result = new \Magento\Framework\DataObject();
        return $result;
    }

    /**
     * Get the sum of height, width and length of all product
     *
     * @param array $items
     * @return array
     */
    protected function getDimensions($items)
    {
        $height = 0;
        $width = 0;
        $length = 0;

        /* @var \Magento\Quote\Model\Quote\Item $item */
        foreach ($items as $item) {
            /** @var \Magento\Catalog\Model\Product $product */
            $product = $this->productRepository->getById($item->getProduct()->getId());

            $dimensions = $this->getProductDimensions($product);
            $height += $dimensions['height'] * $item->getQty();
            $width = ($dimensions['width'] > $width) ? $dimensions['width'] : $width;
            $length = ($dimensions['length'] > $length) ? $dimensions['length'] : $length;
        }

        return array(
            'height' => $height,
            'width' => $width,
            'length' => $length
        );
    }

    /**
     * @param \Magento\Catalog\Model\Product $product
     * @param \Magento\Quote\Model\Quote\Item $item
     * @return int
     */
    protected function getAddDeliveryDays($product, $item, $deadline)
    {
        return 0;
        $avaiabilityAttribute = $this->getConfigData('attribute_add_deadline');
        $alwaysAddTime = $this->getConfigData('always_add_deadline');

        if ($alwaysAddTime) {
            $productDeadline = (int)$product->getResource()->getAttribute($avaiabilityAttribute)->getFrontend()->getValue($product);
            $deadline = ($productDeadline > $deadline) ? $productDeadline : $deadline;
        } else {
            $stock = $this->stockRegistry->getStockItem($product->getId());
            if (($stock->getQty() - $item->getQty()) < 0) {
                $productResource = $product->getResource();
                $productAttribute = $productResource->getAttribute($avaiabilityAttribute);
                $productFrontend = $productAttribute->getFrontend();
                $productDeadLine = (int) $productFrontend->getValue($product);
                //$productDeadline = (int)$product->getResource()->getAttribute($avaiabilityAttribute)->getFrontend()->getValue($product);
                $deadline = ($productDeadline > $deadline) ? $productDeadline : $deadline;
            }
        }

        return $deadline;
    }

    /**
     *
     * @param \Magento\Catalog\Model\Product $product
     * @return array
     */
    public function getProductDimensions(\Magento\Catalog\Model\Product $product)
    {
        $heightAttribute = $this->getConfigData('attribute_height');
        $height = ($heightAttribute) ? $heightAttribute : 'height';

        $widthAttribute = $this->getConfigData('attribute_width');
        $width = ($widthAttribute) ? $widthAttribute : 'width';

        $lengthAttribute = $this->getConfigData('attribute_length');
        $length = ($lengthAttribute) ? $lengthAttribute : 'length';

        $productHeight = (int)$product->getData($height);
        $productWidtht = (int)$product->getData($width);
        $productLength = (int)$product->getData($length);

        $dimensionsType = $this->getConfigData('dimensions_type');
        if ($dimensionsType == 'm') {
            $productHeight = $productHeight * 100;
            $productWidtht = $productWidtht * 100;
            $productLength = $productLength * 100;
        }

        return array(
            'height' => $productHeight,
            'width' => $productWidtht,
            'length' => $productLength
        );
    }

    /**
     * Get result of request
     *
     * @return Result|null
     */
    public function getResult()
    {
        if (!$this->_result) {
            $this->_result = $this->_trackFactory->create();
        }
        return $this->_result;
    }

}
